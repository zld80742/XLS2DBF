﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XLS2DBF
{
    class num2char
    {
        ///Excel列数
        /// 
        /// 数字字母转换
        /// 
        ///

        private string back;

        public string numchar(int num)
        {
            //string back;
            string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (num < 26)
            {
                back = str[num].ToString();
            }
            if (num >= 26)
            {
                int i = num / 26;
                int j = num % 26;
                back = str[i - 1].ToString() + str[j].ToString();
            }
            return this.back;
        }
    }
}
